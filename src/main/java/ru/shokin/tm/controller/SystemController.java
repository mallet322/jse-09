package ru.shokin.tm.controller;

public class SystemController {

    /**
     * Вывод на экран приветствия
     */

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Вывод на эркан информации о доступных командах
     *
     * @return 0
     */

    public int displayHelp() {
        System.out.println("*** INFO PANEL ***");
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Closing the application.");
        System.out.println();
        System.out.println("*** PROJECTS ***");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-list  - Display list of projects.");
        System.out.println("project-view-by-index - Display project by index.");
        System.out.println("project-view-by-id - Display project by id.");
        System.out.println("project-view-by-name - Display project by name.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println("project-update-by-id - Update project by id");
        System.out.println("project-clear - Remove all projects.");
        System.out.println("project-remove-by-index - Remove project by index.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-remove-by-name - Remove project by name");

        System.out.println();
        System.out.println("*** TASKS ***");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-list  - Display list of tasks.");
        System.out.println("task-view-by-index - Display task by index.");
        System.out.println("task-view-by-id - Display task by id.");
        System.out.println("task-view-by-name - Display task by name.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-update-by-id - Update task by id");
        System.out.println("task-clear - Remove all tasks.");
        System.out.println("task-remove-by-index - Remove task by index.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-remove-by-name - Remove task by name.");
        return 0;
    }

    /**
     * Вывод на экран версии приложения
     *
     * @return 0
     */

    public int displayVersion() {
        System.out.println("Current version: 1.0.9");
        return 0;
    }

    /**
     * Вывод на экран сведений о разработчике
     *
     * @return 0
     */

    public int displayAbout() {
        System.out.println("Developer: Elias Shokin");
        System.out.println("   Mailto: shokin_is@nlmk.com");
        return 0;
    }

    /**
     * Вывод на экран сообщения об ошибке
     *
     * @return -1
     */

    public int displayError() {
        System.out.println("Error!!! Unknown program arguments...");
        return -1;
    }

    /**
     * Завершение работы приложения
     *
     * @return 0
     */

    public int displayExit() {
        System.out.println("SEE YOU LATER! :)");
        System.exit(0);
        return 0;
    }

}
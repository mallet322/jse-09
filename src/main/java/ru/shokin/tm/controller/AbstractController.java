package ru.shokin.tm.controller;

import java.util.Scanner;

/**
 * Выносим scanner для того, чтобы Project и Task контроллеры могли от него наследоваться.
 */

public class AbstractController {

    protected final Scanner scanner = new Scanner(System.in);

}
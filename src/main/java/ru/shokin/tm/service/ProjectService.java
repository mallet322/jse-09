package ru.shokin.tm.service;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    /**
     * Слой валидации данных
     */

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findByIndex(int index) {
        if (index > projectRepository.findAll().size() - 1 || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public Project findById(Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project removeByIndex(int index) {
        if (index > projectRepository.findAll().size() - 1 || index < 0) return null;
        return projectRepository.removeByIndex(index);
    }

    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    public Project removeById(Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

}
package ru.shokin.tm;

import static ru.shokin.tm.constant.TerminalConst.*;

import ru.shokin.tm.repository.ProjectRepository;
import ru.shokin.tm.repository.TaskRepository;
import ru.shokin.tm.controller.SystemController;
import ru.shokin.tm.controller.ProjectController;
import ru.shokin.tm.controller.TaskController;
import ru.shokin.tm.service.ProjectService;
import ru.shokin.tm.service.TaskService;

import java.util.Scanner;

/**
 * Тестовое приложение для контроля статусов выполняемых задач
 */

public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final SystemController systemController = new SystemController();

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskController taskController = new TaskController(taskService);


    /**
     * Точка входа
     *
     * @param args параметры запуска
     */

    public static void main(final String[] args) {
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        app.process();
    }

    /**
     * Запуск приложения в режиме бесконечного цикла
     */

    private void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    /**
     * Запуск приложения с аргументом из командной строки
     * Для run используется перегрузка методов
     *
     * @param args - массив аргументов
     */

    private void run(final String[] args) {
        if (args == null || args.length < 1) return;

        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * @param command - выполняемая команда
     * @return код ошибки или 0 в случае успешного завершения
     */

    private int run(final String command) {
        if (command == null || command.isEmpty()) return -1;
        switch (command) {
            case VERSION:
                return systemController.displayVersion();
            case ABOUT:
                return systemController.displayAbout();
            case HELP:
                return systemController.displayHelp();
            case EXIT:
                return systemController.displayExit();

            case PROJECT_CREATE:
                return projectController.createProject();
            case PROJECT_CLEAR:
                return projectController.clearProject();
            case PROJECT_LIST:
                return projectController.listProject();
            case PROJECT_VIEW_BY_INDEX:
                return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return projectController.viewProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById();
            case PROJECT_UPDATE_BY_INDEX:
                return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById();

            case TASK_CREATE:
                return taskController.createTask();
            case TASK_CLEAR:
                return taskController.clearTask();
            case TASK_LIST:
                return taskController.listTask();
            case TASK_VIEW_BY_INDEX:
                return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById();
            case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById();
            case TASK_UPDATE_BY_INDEX:
                return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById();

            default:
                return systemController.displayError();
        }
    }

}
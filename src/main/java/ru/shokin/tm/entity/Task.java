package ru.shokin.tm.entity;

public class Task {

    /**
     * Уникальный идентификатор задачи
     */

    private Long id = System.nanoTime();

    /**
     * Наименование задачи
     */

    private String name = "";

    /**
     * Информация о задачи
     */

    private String description = "";

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task id: " + id + ", Task name: " + name + ", Task description: " + description;
    }

}